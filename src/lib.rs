use std::cmp::{min,max};

pub type DistanceType = i32;

pub trait MonoImage {
    fn is_dark(&self,x:u32,y:u32)->bool;
    fn dimensions(&self)->(u32,u32);
}

pub trait DistanceImage {
    fn new(width: u32, height: u32) -> Self;
    fn set_pixel(&mut self, x: u32, y: u32, signed_distance: DistanceType);
    fn get_pixel(&self, x: u32, y: u32) -> DistanceType;
    fn dimensions(&self)->(u32,u32);
}

#[derive(PartialEq,Eq,Debug)]
pub struct SimpleImageBuffer<T> {
    data: Vec<T>,
    width: u32,
    height: u32,
}

impl<T> SimpleImageBuffer<T>
    where T: Clone, T: Default, T: Copy {
    // index transformation: memory linearisation
    fn idxt(&self,x:u32,y:u32)->usize {
        (y as usize) * (self.width as usize) + (x as usize)
    }
    pub fn new(width: u32, height: u32) -> SimpleImageBuffer<T> {
        let size = (width as usize) * (height as usize);
        let mut data : Vec<T> = Vec::with_capacity(size);
        data.resize(size,T::default());
        SimpleImageBuffer::<T> {
            data: data,
            width: width,
            height: height,
        }
    }
    pub fn set_pixel(&mut self, x: u32, y: u32, value: T) {
        let i = self.idxt(x,y);
        self.data[i] = value;
    }
    pub fn get_pixel(&self, x: u32, y: u32) -> T {
        self.data[self.idxt(x,y)]
    }
    pub fn dimensions(&self)->(u32,u32) {
        (self.width, self.height)
    }
    pub fn from_fn<F>(width: u32, height: u32, f: F) -> SimpleImageBuffer<T> where F: Fn(u32, u32) -> T {
        let mut img = SimpleImageBuffer::<T>::new(width,height);
        for y in 0..height {
            for x in 0..width {
                img.set_pixel(x,y,f(x,y));
            }
        }
        img
    }
}

impl MonoImage for SimpleImageBuffer<bool> {
    fn is_dark(&self,x:u32,y:u32)->bool {
        !self.get_pixel(x,y)
    }
    fn dimensions(&self)->(u32,u32) {
        self.dimensions()
    }
}

impl DistanceImage for SimpleImageBuffer<DistanceType> {
    fn new(width: u32, height: u32) -> Self {
        SimpleImageBuffer::<DistanceType>::new(width,height)
    }
    fn set_pixel(&mut self, x: u32, y: u32, signed_distance: DistanceType) {
        self.set_pixel(x,y,signed_distance)
    }
    fn get_pixel(&self, x: u32, y: u32) -> DistanceType {
        self.get_pixel(x,y)
    }
    fn dimensions(&self)->(u32,u32) {
        self.dimensions()
    }
}

#[cfg(feature = "image")]
extern crate image;

#[cfg(feature = "image")]
pub type DistanceImageBuffer = image::ImageBuffer<image::Luma<DistanceType>, Vec<DistanceType>>;

#[cfg(feature = "image")]
impl MonoImage for image::GrayImage {
    fn is_dark(&self,x:u32,y:u32)->bool {
        self.get_pixel(x,y)[0] < 128
    }
    fn dimensions(&self)->(u32,u32) {
        self.dimensions()
    }
}

#[cfg(feature = "image")]
impl DistanceImage for DistanceImageBuffer {
    fn new(width: u32, height: u32) -> Self {
        DistanceImageBuffer::new(width,height)
    }
    fn set_pixel(&mut self, x: u32, y: u32, signed_distance: DistanceType) {
        self.get_pixel_mut(x,y)[0] = signed_distance;
    }
    fn get_pixel(&self, x: u32, y: u32) -> DistanceType {
        self.get_pixel(x,y)[0]
    }
    fn dimensions(&self)->(u32,u32) {
        self.dimensions()
    }
}

#[cfg(feature = "image")]
pub fn distance_image_to_gray_image<DI:DistanceImage>(inp: &DI, multiplier_option: Option<f64>) -> image::GrayImage {
    fn extreme_value<DI:DistanceImage>(img: &DI) -> DistanceType {
        let (w,h) = img.dimensions();
        let mut extreme = 0;
        for y in 0..h {
            for x in 0..w {
                // think: if pixel < 0 { v = pixel.saturating_neg() }, but it doesn't exist for generig integer types
                let mut v = img.get_pixel(x,y);
                if v == !0 {
                    v = (!0) >> 1;
                } else if v < 0 {
                    v = -v;
                }
                if v > extreme {
                    extreme = v;
                }
            }
        }
        extreme
    }
    let multiplier = multiplier_option.unwrap_or_else(|| i8::max_value() as f64 / extreme_value(inp) as f64);
    assert!(!multiplier.is_nan(), "Multiplier must be a number, but was NaN.");
    let (width,height) = inp.dimensions();
    image::GrayImage::from_fn(width,height,|x,y| {
        let vf = inp.get_pixel(x,y) as f64 * multiplier;
        let v : i32 = if vf < -127_f64 {
            -127
        } else {
            if vf > 127_f64 {
                127
            } else {
                vf as i32
            }
        };
        image::Luma {data:[(v+127) as u8]}
    })
}

#[cfg(feature = "glium")]
extern crate glium;

#[cfg(feature = "glium")]
impl<'a> DistanceImage for glium::texture::RawImage2d<'a, i32> {
    fn new(width: u32, height: u32) -> Self {
        use std::borrow::Cow;
        glium::texture::RawImage2d {
            data: Cow::from(vec![0_i32; (width*height) as usize]),
            width: width,
            height: height,
            format: glium::texture::ClientFormat::I32,
        }
    }
    fn set_pixel(&mut self, x: u32, y: u32, signed_distance: DistanceType) {
        debug_assert!(x < self.width);
        debug_assert!(y < self.height);
        let flipped_y = self.height-1-y;
        self.data.to_mut()[(flipped_y*self.width+x) as usize] = signed_distance;
    }
    fn get_pixel(&self, x: u32, y: u32) -> DistanceType {
        debug_assert!(x < self.width);
        debug_assert!(y < self.height);
        let flipped_y = self.height-1-y;
        self.data[(flipped_y*self.width+x) as usize]
    }
    fn dimensions(&self)->(u32,u32) {
        (self.width,self.height)
    }
}

fn sweep_x <MI: MonoImage, DI: DistanceImage>(img: &MI, forward: bool) -> DI {
    let (width,height) = img.dimensions();
    let mut swept = DI::new(width,height);
    for y in 0..height {
        let mut last_seen : Option<u32> = None;
        let direction : i32 = if forward { 1 } else { -1 };
        let mut x = if forward { 0 } else { width-1 };
        let mut inside = img.is_dark(x,y);
        for travel_index in 0..width {
            if img.is_dark(x,y) != inside {
                last_seen = Some(travel_index-1);
                inside = !inside;
            }
            let v : i32 = match last_seen {
                Some(last_seen) => {
                    debug_assert!(travel_index>=last_seen as u32);
                    let dst = ((travel_index - last_seen) * 2 - 1) as i32;
                    if inside {
                        -dst
                    } else {
                        dst
                    }
                },
                None => 0,
            };
            debug_assert_eq!(v == 0, last_seen.is_none());
            swept.set_pixel(x,y,v);
            x = ((x as i32) + direction) as u32;
        }
    }
    swept
}

fn merge_horizontal_sweeps<DI: DistanceImage>(swept1: &DI, swept2: &DI) -> DI {
    let (width,height) = swept1.dimensions();
    let mut ret = DI::new(width,height);
    for y in 0..height {
        for x in 0..width {
            let v = match (swept1.get_pixel(x,y), swept2.get_pixel(x,y)) {
                (0,0) => 0,
                (a,0) => a,
                (0,b) => b,
                (a,b) => {
                    debug_assert_eq!(a<0,b<0,"disagreement on insideness");
                    if a < 0 {
                        max(a,b)
                    } else {
                        min(a,b)
                    }
                }
            };
            ret.set_pixel(x,y,v);
        }
    }
    ret
}

struct DistanceMipMapEntry {
    best_distance_to_outside: u32,
    best_distance_to_inside: u32,
}

impl DistanceMipMapEntry {
    // note that u32::max_value() means "invalid" here, instead of 0
    fn new_base(distance: i32, added_distance: u32) -> Self {
        if distance == 0 {
            // no data
            DistanceMipMapEntry {
                best_distance_to_outside: u32::max_value(),
                best_distance_to_inside: u32::max_value(),
            }
        } else {
            if distance<0 {
                // inside
                DistanceMipMapEntry {
                    best_distance_to_outside: (-distance) as u32 + added_distance,
                    best_distance_to_inside: u32::max_value(),
                }
            } else {
                // outside
                DistanceMipMapEntry {
                    best_distance_to_inside: distance as u32 + added_distance,
                    best_distance_to_outside: u32::max_value(),
                }
            }
        }
    }
    fn merge(a: &Self, b: &Self) -> Self {
        DistanceMipMapEntry {
            best_distance_to_inside: min(a.best_distance_to_inside, b.best_distance_to_inside),
            best_distance_to_outside: min(a.best_distance_to_outside, b.best_distance_to_outside),
        }
    }
    fn get_best_dst(&self, inside:bool) -> Option<u32> {
        let to_match = if inside {
            self.best_distance_to_outside
        } else {
            self.best_distance_to_inside
        };
        if to_match == u32::max_value() {
            None
        } else {
            Some(to_match)
        }
    }
}

fn log2roundup(x:u32) -> u8 {
    debug_assert!(x > 0, "logarithm of zero is undefined");
    for l in 1u8..33u8 {
        if x >> l == 0 {
            return if x == 1<<(l-1) {
                // x is a power of two, no need to round
                l-1
            } else {
                l
            }
        }
    }
    unreachable!("x >> 32 == 0 for any x:u32");
}

struct DistanceMipMap {
    data: Vec<Vec<DistanceMipMapEntry>>,
    column: i32,
}

impl DistanceMipMap {
    fn new<DI: DistanceImage>(swept: &DI, column: i32) -> DistanceMipMap {
        let (width,height) = swept.dimensions();
        let added_distance = (if column < 0 {
            (-column) as u32
        } else if (column as u32) >= width {
            (column as u32) - (width - 1)
        } else {
            0
        }) * 2;
        let level0sizelog = log2roundup(height);
        let level0size = 1<<level0sizelog;
        let mut l0 = Vec::with_capacity(level0size);
        for i in 0..height {
            l0.push(DistanceMipMapEntry::new_base(swept.get_pixel(clamp(column,width),i), added_distance));
        }
        for _ in (height as usize)..level0size {
            l0.push(DistanceMipMapEntry::new_base(0, added_distance));
        }
        debug_assert_eq!(l0.len(), level0size);
        let mut levels = Vec::with_capacity(level0sizelog as usize);
        levels.push(l0);
        for level in 1..((level0sizelog+1) as usize) {
            let li_size = level0size>>level;
            let mut li = Vec::with_capacity(li_size);
            {
                let lbefore = &levels[level-1];
                for i in 0..lbefore.len()/2 {
                    let v = DistanceMipMapEntry::merge(&lbefore[2*i], &lbefore[2*i+1]);
                    li.push(v);
                }
            }
            debug_assert_eq!(li_size, li.len());
            levels.push(li);
        }
        debug_assert!(levels.last().unwrap().len()==1);
        let ret = DistanceMipMap {
            data: levels,
            column: column,
        };
        // +1 because we also have a level for 2^0=1 pixel
        debug_assert_eq!(ret.get_levels(), level0sizelog+1);
        ret
    }
    fn size_at_level(&self, level: u32) -> u32 {
        self.data[level as usize].len() as u32
    }
    fn best_distance_sqr_rec<MI: MonoImage>(&self, img: &MI, level: u32, entry_id: i32, dst_row: i32, mut best_dst_sqr_so_far: u64, inside: bool) -> u64 {
        let (imgw,imgh) = img.dimensions();
        if entry_id < 0 || entry_id >= (self.size_at_level(level) as i32) {
            // We have no data in that region
            return best_dst_sqr_so_far;
        }
        if level == 0 {
            let thisdstsqr = if img.is_dark(clamp(self.column,imgw), clamp(entry_id,imgh)) != inside {
                // we may be the very pixel that is closest
                let dst = (((entry_id as i32)-(dst_row as i32)).abs() * 2 - 1) as u64;
                dst*dst
            } else {
                match self.data[0][entry_id as usize].get_best_dst(inside) {
                    None => u64::max_value(),
                    Some(side_dst_u32) => {
                        let up_dst = (((if entry_id > dst_row { entry_id - dst_row } else { dst_row - entry_id }) as u64) * 2).saturating_sub(1);
                        let side_dst = side_dst_u32 as u64;
                        side_dst*side_dst + up_dst*up_dst
                    }
                }
            };
            min(thisdstsqr, best_dst_sqr_so_far)
        } else {
            let l0_entry_id_mid = (entry_id*2+1) << level-1;
            let children = if dst_row >= l0_entry_id_mid {
                // check lower child first
                [entry_id*2+1,entry_id*2]
            } else {
                // check upper child first
                [entry_id*2,entry_id*2+1]
            };
            for c in children.iter() {
                let nextlevel_begin = c << (level-1);
                let nextlevel_end = ((c+1) << (level-1)) - 1;
                // attention: The condition of the following if-clause contains a block until somebody comes up with an elegant way to split it up
                if (dst_row < nextlevel_begin) != (dst_row <= nextlevel_end) // destination is IN this child
                    || { let mindst = (min(((nextlevel_begin as i32) - (dst_row as i32)).abs() * 2 - 1,
                                           ((nextlevel_end   as i32) - (dst_row as i32)).abs() * 2 - 1)) as u64;
                         mindst*mindst < best_dst_sqr_so_far // this child could potentially improve the distance altough the dst_row is not inside
                    } {
                    let new_best_dst_sqr = self.best_distance_sqr_rec(img, level-1, *c, dst_row, best_dst_sqr_so_far, inside);
                    debug_assert!(new_best_dst_sqr <= best_dst_sqr_so_far, "The base case must return the original best dst_sqr if no improvement can be made");
                    best_dst_sqr_so_far = new_best_dst_sqr;
                }
            }
            best_dst_sqr_so_far
        }
    }
    fn get_levels(&self) -> u8 {
        self.data.len() as u8
    }
    fn best_distance_sqr<MI: MonoImage>(&self, img: &MI, row: i32, inside: bool) -> u64 {
        self.best_distance_sqr_rec(img, self.get_levels() as u32 - 1, 0, row, u64::max_value(), inside)
    }
}

fn clamp(v: i32, max_v_plus_one: u32) -> u32 {
    if v < 0 {
        0
    } else if (v as u32) >= max_v_plus_one {
        max_v_plus_one - 1
    } else {
        v as u32
    }
}

fn clamp2(pos: (i32,i32), dim: (u32,u32)) -> (u32,u32) {
    let (x,y) = pos;
    let (w,h) = dim;
    (clamp(x,w),clamp(y,h))
}

fn sdf_last_step<MI:MonoImage, DI:DistanceImage>(img: &MI, swept_h: &DI, output_corners: ((i32,i32),(i32,i32)), multiplier: f64) -> DI {
    let (width,height) = swept_h.dimensions();
    let ((out_x0,out_y0),(out_x1,out_y1)) = output_corners;
    let (out_w, out_h) = ((out_x1 - out_x0) as u32, (out_y1 - out_y0) as u32);
    let mut sdfimg = DI::new(out_w, out_h);
    for x in out_x0..out_x1 {
        let mipmap = DistanceMipMap::new(swept_h, x);
        for y in out_y0..out_y1 {
            let (cx,cy) = clamp2((x,y),(width,height));
            let inside = img.is_dark(cx,cy);
            let raw_dst_sqr = mipmap.best_distance_sqr(img,y,inside);
            let v = if raw_dst_sqr == u64::max_value() {
                if inside {
                    i32::min_value()
                } else {
                    i32::max_value()
                }
            } else {
                let f = ((raw_dst_sqr as f64).sqrt() * multiplier) * if inside { -1_f64 } else { 1_f64 };
                if f >= i32::max_value() as f64 {
                    i32::max_value()
                } else if f <= i32::min_value() as f64 {
                    i32::min_value()
                } else {
                    f as i32
                }
            };
            sdfimg.set_pixel((x-out_x0) as u32, (y-out_y0) as u32, v);
        }
    }
    sdfimg
}

#[derive(Debug)]
pub enum SdfError {
    DimensionsTooLarge,
    FlawedGeometry,
}

fn check_size_bounds(output_corners: &((i32,i32),(i32,i32))) -> Result<(),SdfError> {
    let &((x0,y0),(x1,y1)) = output_corners;
    let (width,height) = (x1-x0, y1-y0);
    // distances are multiplied by two because we need half pixel distances for smoothness, so the distance between two pixels is two!
    let multiw = width as u64 * 2;
    let multih = height as u64 * 2;
    let wsqr = try!(multiw.checked_mul(multiw).ok_or(SdfError::DimensionsTooLarge));
    let hsqr = try!(multiw.checked_mul(multih).ok_or(SdfError::DimensionsTooLarge));
    let maxdstsqr = try!(wsqr.checked_add(hsqr).ok_or(SdfError::DimensionsTooLarge));
    let maxdst = (maxdstsqr as f64).sqrt();
    if maxdst > (i32::max_value() as f64) {
        Err(SdfError::DimensionsTooLarge)
    } else {
        Ok(())
    }
}

fn sdf_main<MI:MonoImage, DI:DistanceImage>(img: &MI, output_corners: ((i32,i32),(i32,i32)), multiplier: f64) -> Result<DI,SdfError> {
    try!(check_size_bounds(&output_corners));
    let swept_f = sweep_x::<MI,DI>(img,true);
    let swept_b = sweep_x::<MI,DI>(img,false);
    let swept_h = merge_horizontal_sweeps(&swept_f, &swept_b);
    let sdfimg = sdf_last_step(img, &swept_h, output_corners, multiplier);
    Ok(sdfimg)
}

#[derive(Debug,PartialEq,Eq)]
enum GuardMode {
    NoGuard,
    Border([i32;4]),
    AbsoluteRectangle(i32,i32,i32,i32),
}

// thanks, issue #23121
fn s4_to_t4(s4: &[i32;4]) -> (i32,i32,i32,i32) {
    (s4[0],s4[1],s4[2],s4[3])
}

impl GuardMode {
    fn output_corners(&self, image_dimensions: &(u32,u32)) -> Result<((i32,i32),(i32,i32)),SdfError> {
        use GuardMode::*;
        let (w,h) = match image_dimensions {
            &(uw,uh) => (uw as i32, uh as i32)
        };
        let ((x0,y0),(x1,y1)) = match self {
            &NoGuard => ((0,0),(w,h)),
            &Border(ref s4) => {
                let (r,t,l,b) = s4_to_t4(s4);
                let tl = (try!(l.checked_neg().ok_or(SdfError::DimensionsTooLarge)),  try!(t.checked_neg().ok_or(SdfError::DimensionsTooLarge)));
                let br = (try!(w.checked_add(r).ok_or(SdfError::DimensionsTooLarge)), try!(h.checked_add(b).ok_or(SdfError::DimensionsTooLarge)));
                (tl,br)
            },
            &AbsoluteRectangle(x,y,w,h) => {
                // the outer "w" and "h" are irrelevant in this mode, so we shadow them on purpose
                if w < 1 || h < 1 {
                    // early return to ensure that the right kind of error is returned
                    return Err(SdfError::FlawedGeometry);
                }
                let x1 = try!(x.checked_add(w).ok_or(SdfError::DimensionsTooLarge));
                let y1 = try!(y.checked_add(h).ok_or(SdfError::DimensionsTooLarge));
                ((x,y),(x1,y1))
            },
        };
        if try!(x1.checked_sub(x0).ok_or(SdfError::DimensionsTooLarge)) < 1
            || try!(y1.checked_sub(y0).ok_or(SdfError::DimensionsTooLarge)) < 1 {
            Err(SdfError::FlawedGeometry)
        } else {
            Ok(((x0,y0),(x1,y1)))
        }
    }
}

enum MultiplierMode {
    Fixed(f64),
    GuaranteeFit { max_value: f64 },
}

impl MultiplierMode {
    fn calculate_multiplier(&self, image_dimensions: &(u32,u32), output_corners: &((i32,i32),(i32,i32))) -> Result<f64,SdfError> {
        use MultiplierMode::*;
        let ((x0,y0),(x1,y1)) = *output_corners;
        let (maxx,maxy) = {
            let (dimx,dimy) = *image_dimensions;
            (dimx-1,dimy-1)
        };
        // cds = corner distance squared
        fn cds(x0: u32, y0: u32, x1: i32, y1: i32) -> Result<f64,SdfError> {
            let dx = try!(x1.checked_sub(x0 as i32).ok_or(SdfError::DimensionsTooLarge)) as f64;
            let dy = try!(y1.checked_sub(y0 as i32).ok_or(SdfError::DimensionsTooLarge)) as f64;
            Ok(dx*dx+dy*dy)
        }
        match *self {
            Fixed(c) => Ok(c),
            GuaranteeFit { max_value } => {
                // one of the opposing corners' distances must be the greatest possible
                let distancessqr = [
                    cds(0,0,x1,y1)?,
                    cds(maxx,0,x0,y1)?,
                    cds(0,maxy,x1,y0)?,
                    cds(maxx,maxy,x0,y0)?,
                ];
                let maxdst = distancessqr.iter().fold(0_f64, |a,v| if a>*v { a } else { *v }).sqrt()*2_f64;
                debug_assert!(maxdst > 0_f64);
                if maxdst.is_infinite() {
                    return Err(SdfError::DimensionsTooLarge);
                }
                Ok(max_value / maxdst)
            }
        }
    }
}

pub struct SdfBuilder {
    guard: GuardMode,
    multiplier: MultiplierMode,
}

impl SdfBuilder {
    pub fn new() -> Self {
        SdfBuilder {
            guard: GuardMode::NoGuard,
            multiplier: MultiplierMode::Fixed(2.0),
        }
    }
    pub fn uniform_border (mut self, size: i32) -> Self {
        self.guard = GuardMode::Border([size;4]);
        self
    }
    pub fn non_uniform_border (mut self, right: i32, top: i32, left: i32, bottom: i32) -> Self {
        self.guard = GuardMode::Border([right,top,left,bottom]);
        self
    }
    pub fn fixed_output_rectangle (mut self, x: i32, y: i32, width: i32, height: i32) -> Self {
        self.guard = GuardMode::AbsoluteRectangle(x,y,width,height);
        self
    }
    pub fn constant_multiplier (mut self, multiplier: f64) -> Self {
        self.multiplier = MultiplierMode::Fixed(multiplier);
        self
    }
    pub fn multiplier_fit_i32_range (mut self) -> Self {
        self.multiplier = MultiplierMode::GuaranteeFit {max_value: i32::max_value() as f64};
        self
    }
    pub fn sdf<MI:MonoImage, DI:DistanceImage>(&self, img: &MI) -> Result<DI,SdfError> {
        let corners = try!(self.guard.output_corners(&img.dimensions()));
        let actual_multiplier = try!(self.multiplier.calculate_multiplier(&img.dimensions(), &corners));
        sdf_main(img, corners, actual_multiplier)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn right_dark(width: u32, height: u32) -> SimpleImageBuffer<bool> {
        assert!(width % 2 == 0);
        SimpleImageBuffer::<bool>::from_fn(width,height,|x,_| x<width/2)
    }
    fn down_dark(width: u32, height: u32) -> SimpleImageBuffer<bool> {
        assert!(height % 2 == 0);
        SimpleImageBuffer::<bool>::from_fn(width,height,|_,y| y<height/2)
    }

    #[test]
    fn column_vs_row() {
        let img_row = right_dark(6,1);
        let img_col = down_dark(1,6);
        let sdfimg_row : SimpleImageBuffer<i32> = SdfBuilder::new().sdf(&img_row).unwrap();
        let sdfimg_col : SimpleImageBuffer<i32> = SdfBuilder::new().sdf(&img_col).unwrap();
        assert_eq!(sdfimg_row.data,sdfimg_col.data);
    }

    #[test]
    fn row_only() {
        let (w,h) = (10,3);
        let img = right_dark(w,h);
        let sdfimg : SimpleImageBuffer<i32> = SdfBuilder::new().sdf(&img).unwrap();
        for y in 0..h {
            for x in 0..w/2 {
                let v_here = sdfimg.get_pixel(x,y);
                let v_mirror = sdfimg.get_pixel(w-1-x,y);
                assert!(v_here > 0);
                assert!(v_mirror < 0);
                assert_eq!(v_here, -v_mirror);
                if y > 0 {
                    assert_eq!(v_here, sdfimg.get_pixel(x,y-1));
                }
            }
        }
    }

    #[test]
    fn col_only() {
        let (w,h) = (3,10);
        let img = down_dark(w,h);
        let sdfimg : SimpleImageBuffer<i32> = SdfBuilder::new().sdf(&img).unwrap();
        for y in 0..h/2 {
            for x in 0..w {
                let v_here = sdfimg.get_pixel(x,y);
                let v_mirror = sdfimg.get_pixel(x,h-1-y);
                assert!(v_here > 0);
                assert!(v_mirror < 0);
                assert_eq!(v_here, -v_mirror);
                if x > 0 {
                    assert_eq!(v_here, sdfimg.get_pixel(x-1,y));
                }
            }
        }
    }

    #[test]
    fn all_white() {
        let (w,h) = (7,3);
        let img = SimpleImageBuffer::<bool>::from_fn(w,h,|_,_| true);
        let sdfimg : SimpleImageBuffer<i32> = SdfBuilder::new().sdf(&img).unwrap();
        for y in 0..h {
            for x in 0..w {
                assert_eq!(sdfimg.get_pixel(x,y),i32::max_value());
            }
        }
    }

    #[test]
    fn all_black() {
        let (w,h) = (7,3);
        let img = SimpleImageBuffer::<bool>::from_fn(w,h,|_,_| false);
        let sdfimg : SimpleImageBuffer<i32> = SdfBuilder::new().sdf(&img).unwrap();
        for y in 0..h {
            for x in 0..w {
                assert_eq!(sdfimg.get_pixel(x,y),i32::min_value());
            }
        }
    }

    #[test]
    fn guard_border() {
        let img3 = SimpleImageBuffer::<bool>::from_fn(3,3,|x,y| !(x==1 && y==1));
        let img5 = SimpleImageBuffer::<bool>::from_fn(5,5,|x,y| !(x==2 && y==2));
        let sdf3 : SimpleImageBuffer<i32> = SdfBuilder::new().uniform_border(1).sdf(&img3).unwrap();
        let sdf5 : SimpleImageBuffer<i32> = SdfBuilder::new().sdf(&img5).unwrap();
        assert_eq!(sdf3,sdf5);
    }

    #[cfg(feature = "image")]
    #[test]
    #[ignore]
    fn visual_test() {
        use image;
        let img = image::open("/tmp/test.png").ok().expect("Place a test image at '/tmp/test.png' and look at the output at '/tmp/sdf.png'.").to_luma();
        let sdfimg : DistanceImageBuffer = SdfBuilder::new().uniform_border(128).sdf(&img).unwrap();
        distance_image_to_gray_image(&sdfimg,None).save("/tmp/sdf.png").unwrap();
    }
}
